# tf_aws_audit_guardduty

Configures organization audit account's regional GuardDuty detectors and publishing.

* Creates a GuardDuty detector
* Configures a publishing destination (S3 bucket)


## Variables

| Name                   | Type   | Description                                                                                  | Default    |
|------------------------|--------|----------------------------------------------------------------------------------------------|------------|
| `profile`              | string | Name of AWS profile to use for audit account                                                 | _none_     |
| `bucket_arn`           | string | GuardDuty S3 Bucket Arn                                                                      | _none_     |
| `kms_arn`              | string | GuardDuty KMS Key Arn                                                                        | _none_     |
| `publishing_frequency` | string | Finding publishing frequency; values:<br/>`FIFTEEN_MINUTES`,<br/>`ONE_HOUR`,<br/>`SIX_HOURS` | `ONE_HOUR` |

## Outputs

None.

## Usage

This module must be instantiated in the audit account for each region that requires GuardDuty.

```terraform
module "account" {
  source = "git::https://code.stanford.edu/sc2_public/tf_aws_account_default.git"
  alias  = "org-audit"
}

module "audit" {
  source         = "git::https://code.stanford.edu/sc2_public/tf_aws_audit.git"
  profile        = "org-audit"
  config_regions = [ "us-east-1", "us-east-2", "us-west-1", "us-west-2" ]
  providers      = {
    aws.master    = aws.master
    aws.us_east_1 = aws.us_east_1
    aws.us_east_2 = aws.us_east_2
    aws.us_west_1 = aws.us_west_1
    aws.us_west_2 = aws.us_west_2
  }
}

module "audit_us_west_2" {
  source     = "git::https://code.stanford.edu/sc2_public/tf_aws_audit_guardduty.git"
  profile    = "org-audit"
  bucket_arn = module.audit.guardduty_bucket_arn
  kms_arn    = module.audit.guardduty_kms_arn
  providers  = {
    aws = aws.us_west_2
  }
}

```

## Related Modules

* [AWS Default Setup](https://code.stanford.edu/sc2_public/tf_aws_account_default/)
* [AWS Audit Setup](https://code.stanford.edu/sc2_public/tf_aws_audit/)
* [VPC FlowLogs setup](https://code.stanford.edu/sc2_public/tf_aws_account_flowlogs)
* [Guardduty setup](https://code.stanford.edu/sc2_public/tf_aws_account_guardduty)
