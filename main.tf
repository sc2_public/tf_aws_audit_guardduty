#
# Audit Account GuardDuty regional detector
#

# need a master detector per region
resource "aws_guardduty_detector" "master" {
  enable                       = true
  finding_publishing_frequency = var.publishing_frequency
}

# Terraform doesn't support publishing destinations yet, so
# use the CLI

resource "null_resource" "publishing_destination" {
  triggers = {
    detector_id = aws_guardduty_detector.master.id
  }

  provisioner "local-exec" {
    command = "aws guardduty create-publishing-destination --profile ${var.profile} --region ${data.aws_region.current.name} --detector-id ${aws_guardduty_detector.master.id} --destination-type S3 --destination-properties DestinationArn=${var.bucket_arn},KmsKeyArn=${var.kms_arn}"
  }
}

