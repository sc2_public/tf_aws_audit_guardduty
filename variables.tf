variable "profile" {
  description = "AWS profile for creating publishing destination"
  type        = string
}

variable "publishing_frequency" {
  description = "Finding publishing frequency"
  type        = string
  default     = "ONE_HOUR"
}

variable "bucket_arn" {
  description = "ARN of GuardDuty bucket"
  type        = string
}

variable "kms_arn" {
  description = "ARN of GuardDuty KMS key"
  type        = string
}
